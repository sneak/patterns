package main

// from https://www.youtube.com/watch?v=yeetIgNeIkc&t=1064s

import (
	"log"
	"time"
)

func main() {
	stop := StartTimer("main")
	time.Sleep(1 * time.Second)
	defer stop()
}

func StartTimer(name string) func() {
	t := time.Now()
	log.Println(name, "started")
	return func() {
		d := time.Now().Sub(t)
		log.Println(name, "took", d)
	}
}
